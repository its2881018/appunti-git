# Corso Git e Gitlab

## Installazione di Git
Link per scaricare git: https://git-scm.com/downloads

## Configurazione di Git
Per configurare nome utente e la email si usare:
```bash
git config --global user.name "Il tuo nome"
git config --global user.email "la.tua.email@example.com"
```
## Creazione di un nuovo repository
Per creare un nuovo repository locale:
```bash
git init
```
## Clonazione di un repository
Per copiare un repository remoto sul computer, usarere il comando `git clone` seguito dall'URL del repository.
```bash
git clone https://gitlab.com/username/repository.git
```
## Aggiunta e Commit dei cambiamenti
Per tracciare le modifiche ai file, bisogna prima "aggiungerli" all'area di staging con `git add`.
```bash
git add file1.txt file2.txt
```
O per aggiungere tutti i file nel repository:
```bash
git add .
```
Quindi, per salvare queste modifiche nel tuo repository locale, usare `git commit`.
```bash
git commit -m "Messaggio del commit"
```
## Push dei cambiamenti
Per caricare le modifiche dal tuo repository locale a quello remoto, usare `git push`.
```bash
git push origin master
```
## Pull dei cambiamenti
Per scaricare le modifiche dal repository remoto a quello locale, usare `git pull`.
```bash
git pull origin master
```
## Branching
Per creare un nuovo branch, usare `git branch`.
```bash
git branch nuovo-branch
```
Per passare a un altro branch, usare `git checkout`.
```bash
git checkout nuovo-branch
```
Per creare un nuovo branch e passare ad esso in un unico passaggio, usare `git checkout -b`.
```bash
git checkout -b nuovo-branch
```
## Merge dei branch
Per unire le modifiche da un altro branch nel branch corrente, usare `git merge`.
```bash
git merge altro-branch
```
## Risoluzione dei conflitti
Se ci sono conflitti durante il merge, dovrai risolverli manualmente. Puoi aprire i file con conflitti, cercare le seguenti marcature per vedere dove sono i conflitti:
```bash
your changes
```
Rimuovi le marcature e scegli quale versione mantenere, o crea una nuova versione. Quindi, devi aggiungere e committare il file risolto.
